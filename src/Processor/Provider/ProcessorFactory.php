<?php

namespace App\Processor\Provider;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Log\Logger;
use Exception;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class ProcessorFactory
{
    /**
     * @var FilesystemMap
     */
    private $fileSystemMap;

    /**
     * @var String
     */
    private $fileSystem;

    /**
     * @var ClientManager
     */
    private $client;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @var Logger
     */
    private $logger;


    public function __construct(FilesystemMap $filesystemMap,
                                String $fileSystem,
                                ClientManager $client,
                                EventDispatcherInterface $dispatcher,
                                Logger $logger)
    {
        $this->fileSystemMap = $filesystemMap;
        $this->fileSystem = $fileSystem;
        $this->client = $client;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    /**
     * @param string $file_path
     * @param string $file_type
     * @param string $provider_name
     * @param string $provider_file_type
     * @param string|null $feature
     * @return AbstractProcessor
     * @throws Exception
     */
    public function create(string $file_path, string $file_type, string $provider_name, string $provider_file_type, ?string $feature = null): AbstractProcessor
    {
        $processor_class = 'App\Processor\Provider\\' .
            ucfirst($provider_name) . '\\' .
            ucfirst($file_type) . '\\' .
            (is_null($feature) ? '' : ucfirst($feature) . '\\') .
            ucfirst($provider_file_type);
        if (class_exists($processor_class)) {
            $instance = new $processor_class(
                $this->fileSystemMap->get($this->fileSystem),
                $this->client,
                $this->logger,
                $file_path
            );
            if ($instance instanceof EventDispatcherDependencyInterface) {
                $instance->setEventDispatcher($this->dispatcher);
            }
            if ($instance instanceof AbstractProcessor) {
                return $instance;
            } else {
                $this->logger->error('Processor ' . $processor_class . ' incorrect inheritance');
                throw new Exception('Processor ' . $processor_class . ' incorrect inheritance');
            }
        }
        $this->logger->error('Processor ' . $processor_class . ' does not exist');
        throw new Exception('Processor ' . $processor_class . ' does not exist');
    }
}