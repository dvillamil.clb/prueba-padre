<?php

namespace App\Processor\Provider\Rushbet;

interface ProcessorInterface
{
    public const PROVIDER = 'RUSHBET';
    public const RUSHBET_EVENT = '#event';
}