<?php

namespace App\Processor\Provider\Rushbet\Json;

use App\Processor\Provider\Rushbet\AbstractProcessor as AbstractProcessorRushbet;
use App\Processor\Provider\ProcessorJsonTrait;

abstract class AbstractProcessor extends AbstractProcessorRushbet
{
    use ProcessorJsonTrait;
}