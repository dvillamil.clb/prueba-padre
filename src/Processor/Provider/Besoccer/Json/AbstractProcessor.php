<?php

namespace App\Processor\Provider\Besoccer\Json;

use App\Processor\Provider\Besoccer\AbstractProcessor as AbstractProcessorBesoccer;
use App\Processor\Provider\ProcessorJsonTrait;

abstract class AbstractProcessor extends AbstractProcessorBesoccer
{
    use ProcessorJsonTrait;
}