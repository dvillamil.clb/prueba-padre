<?php

namespace App\Processor\Provider\Traits;

trait MatchIdTrait
{
    /**
     * @var string
     */
    private $matchId;

    /**
     * @return string
     */
    public function getMatchId(): string
    {
        return $this->matchId;
    }

    /**
     * @param string $matchId
     */
    public function setMatchId(string $matchId): void
    {
        $this->matchId = $matchId;
    }
}