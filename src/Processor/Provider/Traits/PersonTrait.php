<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Request as PersonRequest;
use AsResultados\OAMBundle\Exception\MissingItemException;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait PersonTrait
{
    /**
     * @var Person
     */
    private $person;

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @param string $personId
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setPerson(string $personId, ClientManager $client): void
    {
        try {
            $personRequest = new PersonRequest();
            $request = $personRequest->getById($personId);
            $client->clearRequests();
            $client->addRequest($request);
            $client->execute();
            $response = $client->getResponseFor(
                $request,
                [$personRequest::STATUS_CODE_OK, $personRequest::STATUS_CODE_NOT_FOUND]
            );
            $resource = $personRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    $personId,
                    Person::class,
                    $personRequest->getUrl()
                );
            }
            $person = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()]
            );
            $this->person = $person;
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find person with id: ' . $personId . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }
}