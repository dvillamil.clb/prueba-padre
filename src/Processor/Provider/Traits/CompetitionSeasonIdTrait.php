<?php

namespace App\Processor\Provider\Traits;

trait CompetitionSeasonIdTrait
{
    /**
     * @var string
     */
    private $competitionSeasonId;

    /**
     * @return string
     */
    protected function getCompetitionSeasonId(): string
    {
        return $this->competitionSeasonId;
    }

    /**
     * @param string $competitionSeasonId
     */
    protected function setCompetitionSeasonId(string $competitionSeasonId): void
    {
        $this->competitionSeasonId = $competitionSeasonId;
    }
}