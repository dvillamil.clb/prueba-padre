<?php

namespace App\Processor\Provider\Traits;

trait StageIdTrait
{
    /**
     * @var string
     */
    private $stageId;

    /**
     * @return string
     */
    public function getStageId(): string
    {
        return $this->stageId;
    }

    /**
     * @param string $stageId
     */
    public function setStageId(string $stageId): void
    {
        $this->stageId = $stageId;
    }
}