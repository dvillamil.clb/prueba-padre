<?php

namespace App\Processor\Provider\Traits;

trait LanguageIdTrait
{
    /**
     * @var string
     */
    private $languageId;

    /**
     * @return string
     */
    public function getLanguageId(): string
    {
        return $this->languageId;
    }

    /**
     * @param string $languageId
     */
    public function setLanguageId(string $languageId): void
    {
        $this->languageId = $languageId;
    }
}