<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Request as MatchRequest;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait MatchTrait
{
    /**
     * @var Match[]
     */
    private $matches = [];

    /**
     * @return Match[]
     */
    public function getMatches(): array
    {
        return $this->matches;
    }

    /**
     * @param array $matchesIds
     * @param ClientManager $client
     * @throws Exception
     * @throws ExceptionInterface
     */
    protected function setMatchesDetails(array $matchesIds, ClientManager $client): void
    {
        try {
            $matchRequest = new MatchRequest();
            $requestMatches = $matchRequest->getByIdsFieldsIdDetailCompetitionSeasonStageMatchDay($matchesIds);
            $client->clearRequests();
            $client->addRequests($requestMatches);
            $client->execute(true);
            $response = $client->getResponseForMultiple(
                $requestMatches,
                [$matchRequest::STATUS_CODE_OK, $matchRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $matchRequest->getResource();
            $matches = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($matches as $match) {
                if ($match instanceof Match) {
                    $this->matches[$match->getId()] = $match;
                }
            }
        } catch (Exception $e) {
            throw new Exception('Can not find matches details: ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }
}