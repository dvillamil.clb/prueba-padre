<?php

namespace App\Processor\Provider;

use Exception;

interface ProcessorInterface
{
    public const STATS_FLOAT_PRECISION = 2;

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool;

    /**
     * @return string
     */
    public function getFilePath(): string;

    /**
     * @throws Exception
     */
    public function initialize(): void;
}