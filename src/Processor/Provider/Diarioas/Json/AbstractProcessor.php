<?php

namespace App\Processor\Provider\Diarioas\Json;

use App\Processor\Provider\Diarioas\AbstractProcessor as AbstractProcessorDiarioas;
use App\Processor\Provider\ProcessorJsonTrait;

abstract class AbstractProcessor extends AbstractProcessorDiarioas
{
    use ProcessorJsonTrait;
}