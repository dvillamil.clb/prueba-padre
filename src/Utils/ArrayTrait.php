<?php

namespace App\Utils;

trait ArrayTrait
{
    public function excludeNulls(array $array): array
    {
        foreach ($array as $key => $item) {
            is_array($item) && $array[$key] = $this->excludeNulls($item);
            if (is_null($array[$key]))
                unset ($array[$key]);
        }
        return $array;
    }
}