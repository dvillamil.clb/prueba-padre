<?php

namespace App\Utils;

use AsResultados\OAMBundle\Model\Results\Category\Category;

trait FootballTrait
{
    /**
     * @param int $seconds
     * @param Category $period
     * @return int
     */
    protected function getRelativeSecondsForPeriod(int $seconds, Category $period): int
    {
        switch ($period->getId()) {
            case $period::MATCH_PERIOD_FIRST_HALF:
            case $period::MATCH_PERIOD_HALF_TIME:
                return $seconds;
            case $period::MATCH_PERIOD_SECOND_HALF:
            case $period::MATCH_PERIOD_PRE_EXTRA:
                return $seconds - (45 * 60);
            case $period::MATCH_PERIOD_EXTRA_FIRST_HALF:
            case $period::MATCH_PERIOD_EXTRA_HALF_TIME:
                return $seconds - (90 * 60);
            case $period::MATCH_PERIOD_EXTRA_SECOND_HALF:
            case $period::MATCH_PERIOD_PRE_SHOOTOUT:
                return $seconds - ((90 + 15) * 60);
            case $period::MATCH_PERIOD_SHOOT_OUT:
                return $seconds - ((90 + 15 + 15) * 60);
            case $period::MATCH_PERIOD_PRE_MATCH:
            case $period::MATCH_PERIOD_FINISHED:
            default:
                return 0;
        }
    }
}