<?php

namespace App\Api\External\AsNews\Match;

use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\Match\Model\Embed\News;
use App\Api\External\AsNews\Match\Model\Embed\Url;
use App\Api\External\AsNews\ResourceInterface;
use App\Api\External\AsNews\Match\Model\Match;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

final class Resource extends AbstractResource implements ResourceInterface
{
    private $resource;

    /**
     * @inheritDoc
     * @return Match
     */
    public function deserializeOne(array $json, $object = null): Match
    {
        if (is_null($object)) {
            $match = new Match();
        } else {
            $match = $object;
        }
        $match->setId($json['id']);
        $matchUrl = new Url();
        $matchNewsUrlArray = [];
        if (isset($json['enlace'])) {
            foreach ($json['enlace'] as $keyLink => $link) {
                if ($keyLink === 'live') {
                    $matchUrl->setMatch($link);
                } else {
                    $edition = $this->resolveEdition($keyLink);
                    if (is_null($edition)) {
                        continue;
                    }
                    $matchNewsUrl = new News();
                    $matchNewsUrl->setEditionSlug($this->resolveEdition($keyLink));
                    if (isset($link['previa'])) {
                        $matchNewsUrl->setPreMatch($link['previa']);
                    }
                    if (isset($link['retransmision'])) {
                        $matchNewsUrl->setLive($link['retransmision']);
                    }
                    if (isset($link['cronica'])) {
                        $matchNewsUrl->setFinished($link['cronica']);
                    }
                    $matchNewsUrlArray[] = $matchNewsUrl;
                }
            }
        }
        if (!empty($matchNewsUrlArray)) {
            $matchUrl->setNews($matchNewsUrlArray);
        }
        $match->setUrl($matchUrl);
        return $match;
    }

    /**
     * @param string $edition
     * @return string|null
     */
    private function resolveEdition(string $edition): ?string
    {
        switch ($edition) {
            case 'es':
                return 'es';
            case 'es-co':
                return 'colombia';
            case 'es-mx':
                return 'mexico';
            case 'es-cl':
                return 'chile';
            case 'en':
                return 'en';
            case 'es-us':
                return 'us';
            case 'es-ar':
                return 'argentina';
            case 'es-pe':
                return 'peru';
            default:
                return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function deserializeMultiple(array $json, ?array &$objects = null): array
    {
        return parent::deserializeMultiple($json, $objects);
    }

    /**
     * @param string $id
     * @param array $item
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function addResource(string $id, array $item)
    {
        $this->resource[$id] = $this->deserializeOne($item);
    }

    /**
     * @param string $id
     * @return Match
     */
    public function getResource(string $id): ?Match
    {
        $cleanId = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if (isset($this->resource[$cleanId])) {
            return $this->resource[$cleanId];
        }
        return null;
    }
}