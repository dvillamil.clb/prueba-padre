<?php

namespace App\Api\External\AsNews\Tv;

use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\ResourceInterface;
use App\Api\External\AsNews\Tv\Model\Tv;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

final class Resource extends AbstractResource implements ResourceInterface
{
    private $resource;

    /**
     * @inheritDoc
     * @return Tv
     */
    public function deserializeOne(array $json, $object = null): Tv
    {
        if (is_null($object)) {
            $tv = new Tv();
        } else {
            $tv = $object;
        }
        $tv->setId($json['id']);
        $tv->setSlug($json['nombre_normalizado']);
        return $tv;
    }

    /**
     * @inheritDoc
     * @return Tv[]
     */
    public function deserializeMultiple(array $json, ?array &$objects = null): array
    {
        return parent::deserializeMultiple($json, $objects);
    }

    /**
     * @param string $id
     * @param array $item
     * @throws ExceptionInterface
     */
    public function addResource(string $id, array $item)
    {
        $this->resource[$id] = $this->deserializeOne($item);
    }

    /**
     * @param string $id
     * @return Tv
     */
    public function getResource(string $id): ?Tv
    {
        $cleanId = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if (isset($this->resource[$cleanId])) {
            return $this->resource[$cleanId];
        }
        return null;
    }
}