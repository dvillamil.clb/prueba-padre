<?php

namespace App\Api\External\AsNews\Tv;

use App\Api\External\AsNews\RequestInterface;

interface TvInterface extends RequestInterface
{
    public const PATH = 'links.php';
}