<?php

namespace App\Api\External\AsNews;

use AsResultados\OAMBundle\Api\AbstractResource as AbstractResourceMaster;
use AsResultados\OAMBundle\Serializer\SerializerStandard;
use Exception;

abstract class AbstractResource extends AbstractResourceMaster
{
    public const RESPONSE_ITEMS = '';

    public function __construct()
    {
    }

    /**
     * @param array $json
     * @param string $class
     * @param null $object
     * @return mixed
     */
    protected function deserializeNative(array $json, string $class, $object = null)
    {
        $context = array();
        $context['skip_null_values'] = true;
        if (!is_null($object)) {
            $context['object_to_populate'] = $object;
        }
        return SerializerStandard::getSerializer()->deserialize(json_encode($json), $class, 'json', $context);
    }

    /**
     * @inheritDoc
     */
    public function deserializeMultiple(array $json, ?array &$objects = null): array
    {
        if (is_array($objects)) {
            if (count($json) !== count($objects)) {
                throw new Exception('json array and objects does not have the same length');
            }
            reset($objects);
        }
        $result = array();
        foreach ($json as $item) {
            $result[] = $this->deserializeOne($item, is_array($objects) ? current($objects) : null);
            if (is_array($objects)) {
                next($objects);
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getResponseItemsName(): string
    {
        return self::RESPONSE_ITEMS;
    }
}