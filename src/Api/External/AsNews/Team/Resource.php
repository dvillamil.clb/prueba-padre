<?php

namespace App\Api\External\AsNews\Team;

use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\ResourceInterface;
use App\Api\External\AsNews\Team\Model\Embed\Image;
use App\Api\External\AsNews\Team\Model\Embed\Piece;
use App\Api\External\AsNews\Team\Model\Team;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

final class Resource extends AbstractResource implements ResourceInterface
{
    private $resource;

    /**
     * @inheritDoc
     * @return Team
     */
    public function deserializeOne(array $json, $object = null): Team
    {
        if (is_null($object)) {
            $team = new Team();
        } else {
            $team = $object;
        }
        if (!empty($json['tag'])) {
            $team->setTag($json['tag']);
        }
        if (!empty($json['nombre_normalizado'])) {
            $team->setSlug($json['nombre_normalizado']);
        }
        if (!empty($json['nombre_corto'])) {
            $team->setShortName($json['nombre_corto']);
        }
        if (!empty($json['abreviatura'])) {
            $team->setAbbreviation($json['abreviatura']);
        }
        if (isset($json['img']) && !empty($json['img'])) {
            $image = new Image();
            $image->setId($json['id']);
            $team->setImage($image);
        }
        if (isset($json['url']) && !empty($json['url'])) {
            $piece = new Piece();
            $piece->setId($json['url']);
            $team->setPiece($piece);
        }
        return $team;
    }

    /**
     * @inheritDoc
     * @return Team[]
     */
    public function deserializeMultiple(array $json, ?array &$objects = null): array
    {
        return parent::deserializeMultiple($json, $objects);
    }

    /**
     * @param string $id
     * @param array $item
     * @throws ExceptionInterface
     */
    public function addResource(string $id, array $item)
    {
        $this->resource[$id] = $this->deserializeOne($item);
    }

    /**
     * @param string $id
     * @return Team
     */
    public function getResource(string $id): ?Team
    {
        $cleanId = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if (isset($this->resource[$cleanId])) {
            return $this->resource[$cleanId];
        }
        return null;
    }
}