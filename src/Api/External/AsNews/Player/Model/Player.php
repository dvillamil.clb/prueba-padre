<?php

namespace App\Api\External\AsNews\Player\Model;

use App\Api\External\AsNews\Player\Model\Embed\Image;
use App\Api\External\AsNews\Player\Model\Embed\Piece;

class Player
{
    /**
     * @var string|null
     */
    private $tag;

    /**
     * @var Image|null
     */
    private $image;

    /**
     * @var Piece|null
     */
    private $piece;

    /**
     * @var string|null
     */
    private $slug;

    /**
     * @var string|null
     */
    private $shortName;

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @param string|null $tag
     */
    public function setTag(?string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * @param Image|null $image
     */
    public function setImage(?Image $image): void
    {
        $this->image = $image;
    }

    /**
     * @return Piece|null
     */
    public function getPiece(): ?Piece
    {
        return $this->piece;
    }

    /**
     * @param Piece|null $piece
     */
    public function setPiece(?Piece $piece): void
    {
        $this->piece = $piece;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param string|null $shortName
     */
    public function setShortName(?string $shortName): void
    {
        $this->shortName = $shortName;
    }
}