<?php

namespace App\Api\External\AsNews\Player;

use App\Api\External\AsNews\RequestInterface;

interface PlayerInterface extends RequestInterface
{
    public const PATH = 'links.php';
}