<?php

namespace App\EventSubscriber;

use App\Event\AbstractEvent;
use App\Event\ResourceProcessed\Ranking\RankingEvent;
use App\Event\ResourceProcessed\Team\TeamEvent;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Uecode\Bundle\QPushBundle\Provider\ProviderInterface;

/*
 * This event subscriber listens to all events needed notify to Publisher queue
 * */

class NotifyToPublishSubscriber implements EventSubscriberInterface
{
    /**
     * @var ProviderInterface
     */
    private $queue_default;

    /**
     * @var ProviderInterface
     */
    private $queue_background;

    public function __construct(ProviderInterface $queue_default, ProviderInterface $queue_background)
    {
        $this->queue_default = $queue_default;
        $this->queue_background = $queue_background;
    }

    /**
     * Defined all events
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        // return the subscribed events, their methods and priorities
        return array(
            'resource.processed' => 'onResourceProcessed'
        );
    }

    /**
     * Through the event, the properties that are sent to the publisher are restricted
     * @param AbstractEvent $event
     * @throws ReflectionException
     */
    public function onResourceProcessed(AbstractEvent $event): void
    {
        $message = [];
        $reflected = new ReflectionClass($event);
        $properties = $reflected->getProperties();
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $message['attributes'][$property->getName()] = $property->getValue($event);
        }

        $this->getQueue($event)->publish($message['attributes']);
    }

    /**
     * @param AbstractEvent $event
     * @return ProviderInterface
     */
    private function getQueue(AbstractEvent $event): ProviderInterface
    {
        if ($this->isBackgroundQueue($event) === true) {
            return $this->queue_background;
        } else {
            return $this->queue_default;
        }
    }

    /**
     * @param AbstractEvent $event
     * @return bool
     */
    private function isBackgroundQueue(AbstractEvent $event): bool
    {
        if ($event instanceof RankingEvent) {
            return true;
        }
        if ($event instanceof TeamEvent) {
            return true;
        }
        return false;
    }
}