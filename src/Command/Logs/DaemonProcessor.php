<?php

namespace App\Command\Logs;

use ReflectionClass;

class DaemonProcessor
{
    private $process_id;

    public function __construct()
    {
        $this->process_id = uniqid(true);
    }

    public function processRecord(array $record)
    {
        $message = '[' . $this->process_id . ']';

        if (isset($record['extra']['class']))
        {
            $class_name = $this->getClassName($record['extra']['class']);

            if ($class_name)
            {
                $message .= '[' . $this->getClassName($record['extra']['class']) . ']';
            }
        }

        preg_match_all('/\[.+\]/', $record['message'], $matches);

        if ($matches)
        {
            $message           .= implode('', $matches[0]);
            $record['message'] = trim(str_replace($matches[0], '', $record['message']));
        }

        $record['extra']['process_info'] = $message;

        return $record;
    }

    private function getClassName($class)
    {
        $reflected = new ReflectionClass($class);

        return $this->camelCaseToUndescore(str_replace(['Command', 'Processor'], ['', ''], $reflected->getShortName()));
    }

    private function camelCaseToUndescore($str)
    {
        return strtoupper(preg_replace('/([^A-Z])([A-Z])/', "$1_$2", $str));
    }
}