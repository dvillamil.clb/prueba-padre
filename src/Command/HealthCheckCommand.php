<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HealthCheckCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:healthcheck';

    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Checks Health of Container')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command Checks Health of Container...')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('Healthy');
    }
}
