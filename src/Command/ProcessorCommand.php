<?php

namespace App\Command;

use App\Processor\Provider\AbstractProcessor;
use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Exception\MappingBucketException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\ProcessorFactory;
use AsResultados\OAMBundle\Model\Collection\PendingCollection;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class ProcessorCommand extends Command
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $file_path;

    /**
     * @var string
     */
    private $feed_schema;

    /**
     * @var string
     */
    private $provider;

    /**
     * @var string
     */
    private $feed_type;

    /**
     * @var string|null
     */
    private $feature;

    /**
     * @var AbstractProcessor
     */
    private $processor;

    /**
     * @var ProcessorFactory
     */
    private $processor_factory;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ClientManager
     */
    private $clientManager;


    public function __construct(LoggerInterface $techLogger, ProcessorFactory $processor_factory, EventDispatcherInterface $dispatcher, ClientManager $clientManager)
    {
        $this->processor_factory = $processor_factory;
        $this->logger = $techLogger;
        $this->dispatcher = $dispatcher;
        $this->clientManager = $clientManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('batch:processor:run')
            ->setDescription('Process a queued message')
            ->addArgument('file_storage_path', InputArgument::REQUIRED, 'File storage path for parsing')
            ->addArgument('feed_schema', InputArgument::REQUIRED, 'Feed schema')
            ->addArgument('provider', InputArgument::REQUIRED, 'Provider')
            ->addArgument('feed_type', InputArgument::REQUIRED, 'Provider feed type')
            ->addArgument('message_id', InputArgument::OPTIONAL, 'Message Id')
            ->addOption('feature', null, InputOption::VALUE_REQUIRED, 'Special feature');

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws MappingException
     * @throws Exception
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processor = $this->processor_factory->create($this->file_path, $this->feed_schema, $this->provider, $this->feed_type, $this->feature);

        /**
         ** LOGGING TIMINGS **
         */
        $this->logger->info('', ['initializeLog' => true, 'moment' => 'START']);
        $this->logger->info(
            ' MESSAGE_ID: ' . $input->getArgument('message_id') .
            ' | FEED: ' . $input->getArgument('feed_type') .
            ' | PATH: ' . $input->getArgument('file_storage_path') .
            (isset($this->feature) ? ' | FEATURE: ' . $this->feature : '')
        );
        $startProcess = (microtime(true));
        /*******************************************************************/
        $pending = PendingCollection::getInstance();
        try {
            $this->processor->initialize();
            $this->processor->run();
        } catch (MappingException $e) {
            $pending->addFromMapping($e, $this->processor->getFilePath());
            $this->logger->error($e->getMessage());
            throw $e;
        } catch (MappingBucketException $e) {
            foreach ($e->getMappingExceptions() as $mappingException) {
                $pending->addFromMapping($mappingException, $this->processor->getFilePath());
            }
            $this->logger->error($e->getMessage());
            throw $e;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        } catch (Throwable $e) {
            $this->logger->emergency('PHP error: ' . $e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine());
            throw $e;
        } finally {
            $pending->sendPending($this->clientManager);
            $this->logger->info('', ['moment' => 'END', 'startTime' => $startProcess]);
            exit(0);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        declare(ticks=1);

        pcntl_signal(SIGINT, [$this, 'handleSignal']);
        pcntl_signal(SIGTERM, [$this, 'handleSignal']);
        pcntl_signal(SIGHUP, [$this, 'handleSignal']);

        $this->file_path = $input->getArgument('file_storage_path');
        $this->feed_schema = ucfirst($input->getArgument('feed_schema'));
        $this->provider = ucfirst($input->getArgument('provider'));
        $this->feed_type = ucfirst($input->getArgument('feed_type'));
        $this->feature = is_null($input->getOption('feature')) ? null : ucfirst($input->getOption('feature'));


    }

    public function handleSignal($signal)
    {
        switch ($signal) {
            case SIGTERM:
            case SIGINT:
            case SIGHUP:
                $this->logger->critical('PROCESS KILLING!');
                break;
            default:
                break;
        }
    }
}