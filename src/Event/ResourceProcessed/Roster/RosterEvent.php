<?php

namespace App\Event\ResourceProcessed\Roster;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamCompetitionSeasonTrait;

abstract class RosterEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'ROSTER';

    use ParamCompetitionSeasonTrait;
}