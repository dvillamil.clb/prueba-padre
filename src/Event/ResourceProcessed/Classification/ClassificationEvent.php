<?php

namespace App\Event\ResourceProcessed\Classification;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamCompetitionSeasonTrait;
use App\Event\Traits\ParamMatchDayTrait;
use App\Event\Traits\ParamStageTrait;

class ClassificationEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'CLASSIFICATION';

    use ParamCompetitionSeasonTrait;
    use ParamStageTrait;
    use ParamMatchDayTrait;

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, null);
    }
}