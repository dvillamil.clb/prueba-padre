<?php

namespace App\Event\ResourceProcessed;

use App\Event\AbstractEvent;

abstract class AbstractResourceProcessedEvent extends AbstractEvent
{
    private const EVENT_NAME = 'resource.processed';

    /**
     * @var string
     */
    protected $resource_type;

    /**
     * @var string|null
     */
    protected $resource_subtype;

    public function __construct(string $resource_type, ?string $resource_subtype = null)
    {
        $this->resource_type = $resource_type;
        $this->resource_subtype = $resource_subtype;
    }

    /**
     * @return string
     */
    public function getResourceType(): string
    {
        return $this->resource_type;
    }

    /**
     * @return string|null
     */
    public function getResourceSubtype(): ?string
    {
        return $this->resource_subtype;
    }

    /**
     * @inheritDoc
     */
    public function getEventName(): string
    {
        return self::EVENT_NAME;
    }
}