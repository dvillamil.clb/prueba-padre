<?php

namespace App\Event\ResourceProcessed\Calendar;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamCompetitionSeasonTrait;

class CalendarEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'CALENDAR';

    use ParamCompetitionSeasonTrait;

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, null);
    }
}