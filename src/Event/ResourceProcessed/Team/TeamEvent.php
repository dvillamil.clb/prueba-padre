<?php

namespace App\Event\ResourceProcessed\Team;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamSeasonTrait;
use App\Event\Traits\ParamTeamTrait;

abstract class TeamEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'TEAM';

    use ParamTeamTrait;
    use ParamSeasonTrait;
}