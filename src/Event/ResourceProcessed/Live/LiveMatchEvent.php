<?php

namespace App\Event\ResourceProcessed\Live;

class LiveMatchEvent extends LiveEvent
{
    private const RESOURCE_SUBTYPE = 'MATCH';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}