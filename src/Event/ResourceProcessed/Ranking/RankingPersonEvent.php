<?php

namespace App\Event\ResourceProcessed\Ranking;

class RankingPersonEvent extends RankingEvent
{
    private const RESOURCE_SUBTYPE = 'PLAYER';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}