<?php

namespace App\Event\ResourceProcessed\Ranking;


class RankingTeamEvent extends RankingEvent
{
    private const RESOURCE_SUBTYPE = 'TEAM';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}