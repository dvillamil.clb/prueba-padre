<?php

namespace App\Event\Traits;

trait ParamStageTrait
{
    /**
     * @var string|null
     */
    protected $stage;

    /**
     * @return string|null
     */
    public function getStage(): ?string
    {
        return $this->stage;
    }

    /**
     * @param string|null $stage
     */
    public function setStage(?string $stage): void
    {
        $this->stage = $stage;
    }
}